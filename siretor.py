#! /usr/bin/python3

# required modules
import falcon
import psycopg2

class siretor(object):
    def on_get(self, req, resp):
        db = psycopg2.connect("dbname=sirene")
        cur = db.cursor()

        limite_nom = 0.5
        limite_adresse = 0.5

        where = ''
        score = ''
        score_max = 0

        # recherches "exactes" (codes INSEE dep/com et NAF)
        code_commune = req.params.get('code_commune', None)
        if code_commune and len(code_commune) >=2 :
            where = where + cur.mogrify(' AND s.code_commune = %s ', (code_commune,)).decode('utf8')

        siren = req.params.get('siren', None)
        if siren and len(siren) == 9:
            where = where + cur.mogrify(' AND s.siren = %s ', (siren,)).decode('utf8')

        cat = req.params.get('categorie', None)
        if cat and cat != '':
            where = where + cur.mogrify(' AND s.categorie = %s ', (cat,)).decode('utf8')

        actif = req.params.get('actif', None)
        if actif and actif in ['A','F']:
            where = where + cur.mogrify(' AND et.etatadministratifetablissement = %s ', (actif.upper(),)).decode('utf8')

        siege = req.params.get('siege', None)
        if siege and siege in ['O', 'N']:
            where = where + cur.mogrify(' AND et.etablissementsiege = %s ', (siege.upper(),)).decode('utf8')

        # recherches floues (nom et adresse)
        nom = req.params.get('nom',None)
        if nom and nom != '':
            if len(nom.strip()) < 3:
                resp.status = falcon.HTTP_413
                resp.text = '{"erreur": "nom doit avoir au moins 3 caractères"}'
            else:
                nom = cur.mogrify(nom).decode('utf8')
                where = where + cur.mogrify(" AND s.nom <->> %s < %s ", (nom, limite_nom)).decode('utf8')
                score = score + cur.mogrify(" + (s.nom <-> %s)::numeric + (s.nom <->> %s)::numeric + (s.nom <<-> %s)::numeric ", (nom, nom, nom)).decode('utf8')
                score_max = score_max + 3

        adresse = req.params.get('adresse',None)
        if adresse and adresse != '':
            if len(adresse.strip()) < 10:
                resp.status = falcon.HTTP_413
                resp.text = '{"erreur": "adresse doit avoir au moins 10 caractères"}'
            else:
                where = where + cur.mogrify(' AND s.adresse <->> %s < %s ',(adresse, limite_adresse)).decode('utf8')
                score = score + cur.mogrify(" + (s.adresse <-> %s)::numeric + (s.adresse <->> %s)::numeric ", (adresse, adresse)).decode('utf8')
                score_max = score_max + 2
    
        # recherches "commence par" (nom et adresse)
        cp = req.params.get('cp', None)
        if cp and len(cp) >= 2:
            where = where + cur.mogrify(' AND s.cp_ville like %s ',(cp+'%',)).decode('utf8')

        naf = req.params.get('naf',None)
        if naf and naf != '':
            where = where + cur.mogrify(' AND s.apet700 like %s ',(naf+'%',)).decode('utf8')


        # score de 1 par défaut si aucune recherche floue
        if score_max == 0:
            score_max = 1
            score = ''

        if where != '':
            query = """
set statement_timeout=10000;
select json_build_object(
    'source', 'Base SIRENE, INSEE',
    'millesime', '2021-09',
    'licence', 'Licence Ouverte 2.0',
    'query_execution_time_ms', EXTRACT(EPOCH FROM (statement_timestamp()-now())*1000000),
    'count', count(r),
    'result', array_to_json(array_agg(r.j))
    )::text
from (
    select
        json_build_object(
            'siretor_score', round(1-((0 %s) / %s),3),
            'etablissement', row_to_json(et),
            'unitelegale', row_to_json(en)
        ) as j
    from
        siretor s
    join
        siret et on (et.siret = s.siret)
    join
        siren en on (en.siren = s.siren)
    where
        (true %s)
    order by 1-((0 %s) / %s) desc
    limit 10) as r""" % (score, score_max, where, score, score_max)
            cur.execute(query)
            siret = cur.fetchone()

            resp.status = falcon.HTTP_200
            resp.set_header('X-Powered-By', 'siretor')
            resp.set_header('Access-Control-Allow-Origin', '*')
            resp.set_header("Access-Control-Expose-Headers","Access-Control-Allow-Origin")
            resp.set_header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept')
            resp.text = siret[0]
        else:
            resp.status = falcon.HTTP_413
            resp.text = '{"erreur": "aucune critère de recherche indiqué"}'

        db.close()


app = falcon.App()
app.add_route('/siretor', siretor())
