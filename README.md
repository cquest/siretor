# Siretor API - API légère de sirétisation

La sirétisation consiste à rechercher le code SIRET d'un établissement dans la base SIRENE de l'INSEE.

La recherche s'effectue en combinant recherches floues et exactes:
- nom de l'établissement (floue)
- adresse (floue)
- code postal (exacte en "commence par")
- code activité NAF (exacte en "commence par")
- code SIREN de l'unité légale (exacte)
- code commune figurant dans le Code Officiel Géographique de l'INSEE (exacte)
- catégorie d'entreprise (exacte)

On peut aussi limiter les résultats:
- actif=A/F pour n'obtenir que des établissements actifs ou fermés
- siege=true/false pour n'obtenir que des établissements siège ou non

## Architecture

Les données géocodées de la base SIRENE sont importées dans une base postgresql.

Les recherches textuelles floues (nom, adresse) s'appuient sur les trigrammes fournis par l'extension pg_trgm de postgresql.

Les requêtes SQL sont préparées par le code python, qui expose l'API à l'aide de falcon et gunicorn.

## Installation

Télécharger et importer les données dans postgresql à l'aide de :

`bash siretor.sh`

Lancer l'API via gunicorn à l'aide de :

`gunicorn siretor:app`

L'API répond par défaut sur http://localhost:8000/siretor


## Utilisation de l'API

Recherche par nom approximatif d'établissement ou entreprise sur une commune connue:
- http://localhost:8000/siretor?nom=petit+duc&code_commune=94068

Recherche par nom et adresse approximatif sur une commune connue:
- http://localhost:8000/siretor?nom=petit+duc&adresse=avenue+charles+de+gaule&code_commune=94068

Recherche par nom approximatif et code NAF partiel sur une commune connue:
- http://localhost:8000/siretor?nom=petit+duc&naf=10&code_commune=94068


Les noms des champs retournés ainsi que leur contenu sont ceux de la base SIRENE de l'INSEE.

## Todo

- ajouter un critère de distance géographique
- migrer vers fast-api pour auto-générer l'interface swagger
