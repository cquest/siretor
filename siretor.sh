export PGDATABASE=siretor

wget -nv -N http://files.data.gouv.fr/insee-sirene/StockUniteLegale_utf8.zip &
wget -nv -N http://data.cquest.org/geo_sirene/v2019/last/StockEtablissement_utf8_geo.csv.gz
wait

# création de la table SIREN (entreprises)
psql -c "drop table if exists siren_temp; create table siren_temp (`unzip -p StockUniteLegale_utf8.zip | head -n 1 | sed 's/,/ text,/g;s/$/ text/'`);"
unzip -p StockUniteLegale_utf8.zip | psql $DB -c "\copy siren_temp from stdin with (format csv, header true)"

# création de la table des établissements
psql -c "drop table if exists siret_temp; create table siret_temp (`zcat StockEtablissement_utf8_geo.csv.gz | head -n 1 | sed 's/,/ text,/g;s/$/ text/'`);"
zcat StockEtablissement_utf8_geo.csv.gz | psql -c "\copy siret_temp from stdin with (format csv, header true)"

# mise à jour live (transaction) de la table "siren" avec optimisation: clustering de la table sur SIREN
psql -c "
-- extensions postgresql nécessaires
create extension if not exists pg_trgm;
create extension if not exists postgis;


begin;
-- définition des tables finales à partir des temporaires
create table if not exists siren as (select * from siren_temp limit 1);
create table if not exists siret as (select * from siret_temp limit 1);

-- on vide les tables finales pour leur mise à jour
truncate siren;
truncate siret;

-- création d'une vue matérialisée pour l'indexation
create materialized view if not exists siretor as
select
    et.siren,
    et.siret,
    activiteprincipaleetablissement as apet700,
    categorieentreprise as categorie,
    trim(regexp_replace(format('%s / %s / %s %s %s / %s / %s',
        denominationunitelegale,
        denominationusuelle1unitelegale,
        nomunitelegale,prenom1unitelegale,prenom2unitelegale,
        denominationusuelleetablissement,
        enseigne1etablissement
        ), ' */ *',' ','g')) as nom,
    format('%s%s %s %s - %s',
        numerovoieetablissement, indicerepetitionetablissement, typevoieetablissement, libellevoieetablissement,
        geo_adresse
    ) as adresse,
    format('%s %s', codepostaletablissement, libellecommuneetablissement) as cp_ville,
    codecommuneetablissement as code_commune,
    st_setsrid(ST_makepoint(longitude::numeric, latitude::numeric),4326) as geom,
    etatadministratifetablissement as etat
from siret et
join siren en on (en.siren=et.siren)
order by code_commune;

-- remplissage des tables finales
insert into siren select * from siren_temp order by siren;
insert into siret select * from siret_temp order by siret;

-- mise à jour de la vue matérialisée
refresh materialized view siretor;

-- suppression des tables temporaires
drop table siren_temp;
drop table siret_temp;

-- validatation de la transaction
commit;
"

echo "######## Indexation en cours"
psql -c "CREATE INDEX if not exists siren_siren on siren using brin (siren);" &
psql -c "CREATE INDEX if not exists siret_siren on siret using brin (siren);" &
psql -c "CREATE INDEX if not exists siret_siret on siret using brin (siret);" &
psql -c "CREATE INDEX if not exists siretor_code_commune ON siretor USING BRIN (code_commune) ;" &
psql -c "CREATE INDEX if not exists siretor_siren ON siretor (siren) ;" &
psql -c "CREATE INDEX if not exists siretor_apet700 ON siretor (apet700) ;" &
psql -c "CREATE INDEX if not exists siretor_categorie ON siretor (categorie) ;" &
psql -c "CREATE INDEX if not exists siretor_nom ON siretor USING GIST (nom gist_trgm_ops) ;" &
psql -c "CREATE INDEX if not exists siretor_adresse ON siretor USING GIST (adresse gist_trgm_ops) ;" &
psql -c "CREATE INDEX if not exists siretor_cp_ville ON siretor USING GIST (cp_ville gist_trgm_ops) ;" &
psql -c "CREATE INDEX if not exists siretor_geom ON siretor USING GIST (geom) ;" &
wait
echo "######## Import/mise à jour et indexation terminés"
